#include "hudcalendar.h"

//======================================== HUDCalendarEntry =================================================
HUDCalendarEntry::HUDCalendarEntry(QQuickItem* parent) : HUDElement(parent)
{
}

HUDCalendarEntry::~HUDCalendarEntry()
{
}

void HUDCalendarEntry::paint(QPainter* painter)
{
}


//======================================== HUDCalendarDay =================================================

HUDCalendarDay::HUDCalendarDay(QQuickItem* parent) : HUDElement(parent)
{
}

HUDCalendarDay::~HUDCalendarDay()
{
}

void HUDCalendarDay::paint(QPainter* painter)
{
}

//======================================== HUDCalendar =================================================

HUDCalendar::HUDCalendar(QQuickItem* parent) : HUDElement(parent)
{
}

HUDCalendar::~HUDCalendar()
{
}

void HUDCalendar::calculateInternalGeometry()
{
}

void HUDCalendar::paint(QPainter* painter)
{
    
}


