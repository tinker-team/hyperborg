Source: hyperborg
Section: net
Priority: optional
Maintainer: Tinker Team <blend-tinker-devel@alioth-lists.debian.net>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Build-Depends:
 cmake,
 cmark-gfm <!nodoc>,
 debhelper-compat (= 13),
 libqt6opengl6-dev,
 libxkbcommon-dev,
 qmake6,
 qt6-charts-dev,
 qt6-declarative-dev,
 qt6-serialport-dev,
 qt6-speech-dev,
 qt6-speech-flite-plugin,
 qt6-websockets-dev,
 qml6-module-qtcharts,
Standards-Version: 4.6.2
Homepage: https://hyperborg.com/
Vcs-Git: https://salsa.debian.org/tinker-team/hyperborg.git
Vcs-Browser: https://salsa.debian.org/tinker-team/hyperborg
Rules-Requires-Root: no

Package: hyperborg
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: decentralised and distributed home automation system (and framework)
 HyperBorg is a framework for decentralised and distributed systems
 mainly used for home automation.
 It provides nodes
 the sensors as well as other data processing elements
 (like SQL servers, file repositories)
 can be attached to.
 These attachment are the "plugins".
 The HyperBorg concept is
 that all information should be available on all nodes,
 regardless of their origin.
 On top of that these kind of events can be processed decentralised
 by an user created program.
 For the simplicity,
 this kind of programming could be achieved
 via Scratch-like programming interface.
 .
 The communication between the nodes are done via websocket,
 that provides fast communication for plugins needing throughput
 (video doorbells and so).
 .
 The control interface is a Linux desktop application
 (KDE traybutton)
 or a WebAssembly application.
 .
 Example of usages:
  1. House management:
     PI #1 controls heating,
     PI #2 controls the system via PI TFT touchscreen,
     PI#3 saves all information into SQL
  2. Server monitoring:
     Server #1 & #2 has node collecting various information
     (df, hddtemp)
     Linux tray client show graphs (and alarm),
     PI devices saves status information into SQL
